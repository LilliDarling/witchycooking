from django.shortcuts import render, redirect
from django.core.mail import send_mail
from django.conf import settings
from legal.forms import ContactForm
from django.contrib import messages
from recipes.models import Recipe
from recipes.forms import SearchForm
from django.db.models import Q


def about(request):
    search_form = SearchForm()
    query = request.GET.get("query")
    recipes = Recipe.objects.all()

    if query:
        recipes = recipes.filter(
            Q(title__icontains=query) | Q(author__username__icontains=query)
        )
        if recipes.exists():
            return redirect("detail", id=recipes.first().id)
    context = {
        "search_form": search_form,
    }
    return render(request, "legal/about.html", context)


def privacy(request):
    search_form = SearchForm()
    query = request.GET.get("query")
    recipes = Recipe.objects.all()

    if query:
        recipes = recipes.filter(
            Q(title__icontains=query) | Q(author__username__icontains=query)
        )
        if recipes.exists():
            return redirect("detail", id=recipes.first().id)
    context = {
        "search_form": search_form,
    }
    return render(request, "legal/privacy.html", context)


def terms_of_service(request):
    search_form = SearchForm()
    query = request.GET.get("query")
    recipes = Recipe.objects.all()

    if query:
        recipes = recipes.filter(
            Q(title__icontains=query) | Q(author__username__icontains=query)
        )
        if recipes.exists():
            return redirect("detail", id=recipes.first().id)
    context = {
        "search_form": search_form,
    }
    return render(request, "legal/terms.html", context)


def contact(request):
    search_form = SearchForm()
    query = request.GET.get("query")
    recipes = Recipe.objects.all()

    if query:
        recipes = recipes.filter(
            Q(title__icontains=query) | Q(author__username__icontains=query)
        )
        if recipes.exists():
            return redirect("detail", id=recipes.first().id)
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data["name"]
            user_email = form.cleaned_data["email"]
            subject = form.cleaned_data["subject"]
            message = form.cleaned_data["message"]

            email_subject = f"Message from {name} via Contact Form"
            email_body = f"Sender's email: {user_email}\n\nSubject:\n{subject}\n\nMessage:\n{message}"
            from_email = settings.EMAIL_HOST_USER

            send_mail(
                email_subject, email_body, from_email, ["lillith@valkyrieremedy.com"]
            )

            messages.success(request, "Your email has been sent successfully!")
            return redirect("contact")
    else:
        form = ContactForm()
    context = {
        "form": form,
        "search_form": search_form,
    }
    return render(request, "legal/contact.html", context)


def guidelines(request):
    search_form = SearchForm()
    query = request.GET.get("query")
    recipes = Recipe.objects.all()

    if query:
        recipes = recipes.filter(
            Q(title__icontains=query) | Q(author__username__icontains=query)
        )
        if recipes.exists():
            return redirect("detail", id=recipes.first().id)
    context = {
        "search_form": search_form,
    }
    return render(request, "legal/guidelines.html", context)
