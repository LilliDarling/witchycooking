from django.urls import path
from legal.views import about, privacy, terms_of_service, contact, guidelines

urlpatterns = [
    path("about/", about, name="about"),
    path("privacy/", privacy, name="privacy"),
    path("terms_of_service/", terms_of_service, name="terms"),
    path("contact/", contact, name="contact"),
    path("guidelines/", guidelines, name="guidelines"),
]
