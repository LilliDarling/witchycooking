from django import forms
from django.core.validators import EmailValidator
from validate_email_address import validate_email


def validate_existing_email(email):
    if not validate_email(email):
        raise forms.ValidationError("This email address does not exist.")


class ContactForm(forms.Form):
    name = forms.CharField(
        max_length=50,
        label="",
        widget=forms.TextInput(attrs={"placeholder": "Name or Username"}),
    )
    email = forms.EmailField(
        validators=[EmailValidator(), validate_existing_email],
        label="",
        widget=forms.TextInput(attrs={"placeholder": "Email"}),
    )
    subject = forms.CharField(
        max_length=150,
        label="",
        widget=forms.TextInput(attrs={"placeholder": "Subject"}),
    )
    message = forms.CharField(
        label="", widget=forms.Textarea(attrs={"placeholder": "Add a message..."})
    )
