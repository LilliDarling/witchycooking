
![App Logo](assets/WCLogo.png)

This project was originally built in an intensive program for software development called Hack Reactor.
After the initial build I took the project and expanded on it by adding elements such as a contact page, the ability
to favorite recipes that aren't your own, adjusting the create and edit functions with javascript to make them more
dynamic, as well as using css at the very base level without bootstrap or tailwind. This project is now designed to 
allow people the option to save recipes and utilize a site without all the fluff and ads that are common on most recipe
sites.

This site is built using Django, Python, JavaScript, HTML, CSS, with a transition into Docker, and deployed using OceanDigital. I chose to deploy the static files and uploaded images using the same bucket with Google Cloud so that I 
can eventually also add in the Google Translation API to give more accessibility. 

## Features

- (Working on) Translation API
- Signup/(working on) Account Management
- Create, Edit, Save recipes

## How to run this App in developement

### Prerequisites

Make sure you have Docker Desktop installed on your device and updated. I suggest you get this up and running first.  
Also check to see if you have an IDE like VSCode installed on your desktop. Know that some things will need to be reconfigured with your own enviornment variable file. Currently, this project only has a deployment setup so some things 
will not function as expected.

### SetUp

- Go to https://gitlab.com/LilliDarling/witchycooking and Fork the repository  
- In your Terminal on your desktop, navigate into your project directory  
- In YOUR gitlab, click on the blue CODE drop down and clone your repository  
- Back in the terminal where you navigated to your projects directory, use git clone and the repository you just copied  
- Navigate into the cloned project and run the following commands in your terminal:  
    - docker volume create witchycookingdb 
    - docker compose build  
    - docker compose up  
    - code .  
- Get familiar with the code. When you want to view through the web server, navigate to localhost:8000 or go into your Docker Desktop app, find the web container, and click on the 8000:8000 button that will be blue.  

If you are wanting to add in your own take to the code, I suggest creating a dev branch with your name to push any changes to until you are happy with your code and wanting to merge.

### Contributing

1. Fork the repository.
2. Create a new branch (`git checkout -b feature/your-feature`).
3. Make your changes.
4. Commit your changes (`git commit -m 'Add some feature'`).
5. Push to the branch (`git push origin feature/your-feature`).
6. Open a Pull Request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Contact

Lillith - [info@valkyrieremedy.com](mailto:info@valkyrieremedy.com)

Project Link: [https://witchycooking.com](https://witchycooking.com)
