from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import EmailValidator
import re
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from validate_email_address import validate_email


def validate_password(value):
    if not re.match(r"^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*?~/]).{8,}$", value):
        raise ValidationError(
            "Password must be at least 8 characters long and contain at least one uppercase letter, one number, and one special character."
        )


class SignUpForm(forms.Form):
    username = forms.CharField(
        max_length=25,
        label="",
        widget=forms.TextInput(attrs={"placeholder": "Username"}),
    )
    first_name = forms.CharField(
        max_length=100,
        label="",
        widget=forms.TextInput(attrs={"placeholder": "First Name"}),
    )
    last_name = forms.CharField(
        max_length=100,
        label="",
        widget=forms.TextInput(attrs={"placeholder": "Last Name"}),
    )
    email = forms.EmailField(
        validators=[EmailValidator(), validate_email],
        label="",
        widget=forms.TextInput(attrs={"placeholder": "Email"}),
    )
    password = forms.CharField(
        max_length=20,
        widget=forms.PasswordInput(attrs={"placeholder": "Password"}),
        label="",
        validators=[validate_password],
    )
    password_confirmation = forms.CharField(
        max_length=20,
        widget=forms.PasswordInput(attrs={"placeholder": "Confirm Password"}),
        label="",
    )

    def clean_username(self):
        username = self.cleaned_data.get("username")
        if User.objects.filter(username=username).exists():
            raise ValidationError(
                "This username is already taken. Please choose a different one."
            )
        return username

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        password_confirmation = cleaned_data.get("password_confirmation")

        if password and password_confirmation and password != password_confirmation:
            self.add_error("password_confirmation", "Passwords do not match.")
        return cleaned_data

    def save(self):
        username = self.cleaned_data["username"]
        first_name = self.cleaned_data["first_name"]
        last_name = self.cleaned_data["last_name"]
        password = self.cleaned_data["password"]
        hashed_password = make_password(password)
        user = User.objects.create(
            username=username,
            first_name=first_name,
            last_name=last_name,
            password=hashed_password,
        )
        return user


class LoginForm(forms.Form):
    username = forms.CharField(
        max_length=25,
        label="",
        widget=forms.TextInput(attrs={"placeholder": "Username"}),
    )
    password = forms.CharField(
        max_length=20,
        widget=forms.PasswordInput(attrs={"placeholder": "Password"}),
        label="",
    )


class PasswordResetRequestForm(forms.Form):
    email = forms.EmailField(
        label="", widget=forms.EmailInput(attrs={"placeholder": "Email"})
    )

    def clean_email(self):
        email = self.cleaned_data["email"]
        if not User.objects.filter(email=email).exists():
            raise ValidationError("This email is not registered.")
        return email


class PasswordResetForm(forms.Form):
    password = forms.CharField(
        max_length=20,
        widget=forms.PasswordInput(attrs={"placeholder": "New Password"}),
        label="",
        validators=[validate_password],
    )
    password_confirmation = forms.CharField(
        max_length=20,
        widget=forms.PasswordInput(attrs={"placeholder": "Confirm New Password"}),
        label="",
    )

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        password_confirmation = cleaned_data.get("password_confirmation")
        if password and password_confirmation and password != password_confirmation:
            self.add_error("password_confirmation", "Passwords do not match.")
        return cleaned_data
