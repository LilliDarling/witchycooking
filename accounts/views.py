from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from accounts.forms import (
    SignUpForm,
    LoginForm,
    PasswordResetRequestForm,
    PasswordResetForm,
)
from recipes.forms import SearchForm
from recipes.models import Recipe
from django.db.models import Q
from django.utils.crypto import get_random_string
from django.core.mail import send_mail
from django.contrib import messages
from accounts.models import PasswordResetToken


def signup(request):
    search_form = SearchForm()
    query = request.GET.get("query")
    recipes = Recipe.objects.all()

    if query:
        recipes = recipes.filter(
            Q(title__icontains=query) | Q(author__username__icontains=query)
        )
        if recipes.exists():
            return redirect("detail", id=recipes.first().id)

    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            first_name = form.cleaned_data["first_name"]
            last_name = form.cleaned_data["last_name"]
            email = form.cleaned_data["email"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    email=email,
                    first_name=first_name,
                    last_name=last_name,
                    password=password,
                )
                login(request, user)
                return redirect("all")
            else:
                form.add_error("password", "Passwords do not match")
    else:
        form = SignUpForm()

    context = {
        "form": form,
        "search_form": search_form,
    }

    return render(request, "accounts/signup.html", context)


def user_login(request):
    search_form = SearchForm()
    query = request.GET.get("query")
    recipes = Recipe.objects.all()

    if query:
        recipes = recipes.filter(
            Q(title__icontains=query) | Q(author__username__icontains=query)
        )
        if recipes.exists():
            return redirect("detail", id=recipes.first().id)

    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("all")
    else:
        form = LoginForm()

    context = {
        "form": form,
        "search_form": search_form,
    }

    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("all")


def password_reset_request(request):
    if request.method == "POST":
        form = PasswordResetRequestForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data["email"]
            user = User.objects.filter(email=email).first()
            if user:
                token = get_random_string(length=32)
                PasswordResetToken.objects.create(user=user, token=token)
                user.save()
                reset_link = f"http://localhost:8000/accounts/reset-password/{token}/"
                send_mail(
                    "Password Reset Request",
                    f"Click the following link to reset your password: {reset_link}",
                    "from@example.com",
                    [email],
                )
                messages.success(
                    request, "Password reset link has been sent to your email."
                )
                return redirect("login")
            else:
                messages.error(request, "No user found with the provided email.")
    else:
        form = PasswordResetRequestForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/request.html", context)


def password_reset(request, token):
    password_reset_token = PasswordResetToken.objects.filter(token=token).first()
    if password_reset_token:
        user = password_reset_token.user
        if request.method == "POST":
            form = PasswordResetForm(request.POST)
            if form.is_valid():
                password = form.cleaned_data["password"]
                user.set_password(password)
                password_reset_token.delete()
                user.save()

                messages.success(request, "Password has been reset!")
                return redirect("login")
        else:
            form = PasswordResetForm()
        context = {
            "form": form,
        }
        return render(request, "accounts/reset.html", context)
    else:
        messages.error(request, "Invalid password reset token.")
        return redirect("login")
