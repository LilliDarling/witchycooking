from django.urls import path
from accounts.views import (
    signup,
    user_login,
    user_logout,
    password_reset_request,
    password_reset,
)

urlpatterns = [
    path("signup/", signup, name="signup"),
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("password-reset/", password_reset_request, name="password_reset_request"),
    path("reset-password/<str:token>/", password_reset, name="password_reset"),
]
