# Select the base image that is best for our application
FROM python:3.11

# Set the working directory to copy stuff to
WORKDIR /app

# Copy all the code from the local directory into the image
COPY . .
COPY . /app
COPY manage.py manage.py

# Install any language dependencies
RUN pip install --no-cache-dir -r requirements.txt
RUN apt-get update && apt-get install -y netcat-openbsd
RUN apt-get update && apt-get install -y nano

# Run Django Migrations to create the necessary tables in the database
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

# Collect static files
RUN python manage.py collectstatic --noinput

# Expose the default Django port
EXPOSE 8000

# Set the command to run the application
CMD gunicorn --bind 0.0.0.0:8000 scrumptious.wsgi