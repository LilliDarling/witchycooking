from django.db import models
from django.conf import settings
from django.contrib.auth.models import User


class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.ImageField(upload_to="recipe_pictures", blank=True, null=True)
    description = models.TextField(default="Default Text")
    servings = models.PositiveIntegerField(default=1)
    calories = models.PositiveIntegerField(default=0)
    protein = models.PositiveIntegerField(default=0)
    fat = models.PositiveIntegerField(default=0)
    carbs = models.PositiveIntegerField(default=0)
    created_on = models.DateField(auto_now_add=True)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title


class RecipeSteps(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(Recipe, related_name="steps", on_delete=models.CASCADE)

    class Meta:
        ordering = ["step_number"]


class Ingredients(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        Recipe, related_name="ingredients", on_delete=models.CASCADE
    )

    class Meta:
        ordering = ["food_item"]


class Comment(models.Model):
    recipe = models.ForeignKey(
        Recipe, related_name="comments", on_delete=models.CASCADE
    )
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Comment by {self.author} on {self.recipe}"


class FavoriteRecipes(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="favorite_recipes",
    )
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)

    class Meta:
        unique_together = ("user", "recipe")

    def __str__(self):
        return f"{self.user.username} - {self.recipe.title}"
