from django.shortcuts import get_object_or_404, render, redirect
from recipes.models import Recipe, RecipeSteps, Ingredients, FavoriteRecipes
from recipes.forms import (
    RecipeForm,
    IngredientsForm,
    RecipeStepsForm,
    SearchForm,
    CommentForm,
)
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.forms import modelformset_factory
from django.db.models import Q
from django.utils import timezone


def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    search_form = SearchForm()
    comment_form = CommentForm()
    query = request.GET.get("query")
    recipes = Recipe.objects.all()

    if query:
        recipes = recipes.filter(
            Q(title__icontains=query) | Q(author__username__icontains=query)
        )
        if recipes.exists():
            return redirect("detail", id=recipes.first().id)
    if request.method == "POST":
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.recipe = recipe
            comment.author = request.user
            comment.created_on = timezone.now()
            comment.save()
            return redirect("detail", id=recipe.id)

    context = {
        "recipe_object": recipe,
        "search_form": search_form,
        "comment_form": comment_form,
    }

    return render(request, "recipes/details.html", context)


def recipe_list(request):
    search_form = SearchForm()
    query = request.GET.get("query")
    recipes = Recipe.objects.all()

    if query:
        recipes = recipes.filter(
            Q(title__icontains=query) | Q(author__username__icontains=query)
        )
        if recipes.exists():
            return redirect("detail", id=recipes.first().id)
    context = {
        "recipe_list": recipes,
        "search_form": search_form,
    }
    return render(request, "recipes/list.html", context)


@login_required
def my_recipes(request):
    recipes = Recipe.objects.filter(author=request.user)
    search_form = SearchForm()
    query = request.GET.get("query")

    if query:
        recipes = recipes.filter(
            Q(title__icontains=query) | Q(author__username__icontains=query)
        )
        if recipes.exists():
            return redirect("detail", id=recipes.first().id)
    context = {
        "recipe_list": recipes,
        "search_form": search_form,
    }
    return render(request, "recipes/list.html", context)


@login_required
def create_recipe(request):
    search_form = SearchForm()
    query = request.GET.get("query")
    recipes = Recipe.objects.all()

    if query:
        recipes = recipes.filter(
            Q(title__icontains=query) | Q(author__username__icontains=query)
        )
        if recipes.exists():
            return redirect("detail", id=recipes.first().id)

    IngredientsFormSet = modelformset_factory(
        Ingredients,
        form=IngredientsForm,
        extra=1,
        can_delete=True,
        fields=["amount", "food_item"],
    )
    RecipeStepsFormSet = modelformset_factory(
        RecipeSteps,
        form=RecipeStepsForm,
        extra=1,
        can_delete=True,
        fields=["step_number", "instruction"],
    )

    if request.method == "POST":
        form = RecipeForm(request.POST, request.FILES)
        ingredient_formset = IngredientsFormSet(
            request.POST, queryset=Ingredients.objects.none(), prefix="ingredients"
        )
        steps_formset = RecipeStepsFormSet(
            request.POST, queryset=RecipeSteps.objects.none(), prefix="steps"
        )

        if (
            form.is_valid()
            and ingredient_formset.is_valid()
            and steps_formset.is_valid()
        ):
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()

            for ingredient_form in ingredient_formset:
                if (
                    ingredient_form.cleaned_data
                    and not ingredient_form.cleaned_data.get("DELETE")
                ):
                    ingredient = ingredient_form.save(False)
                    ingredient.recipe = recipe
                    ingredient.save()

            for steps_form in steps_formset:
                if steps_form.cleaned_data and not steps_form.cleaned_data.get(
                    "DELETE"
                ):
                    step = steps_form.save(False)
                    step.recipe = recipe
                    step.save()

            return redirect("all")

    else:
        form = RecipeForm()
        ingredient_formset = IngredientsFormSet(
            queryset=Ingredients.objects.none(), prefix="ingredients"
        )
        steps_formset = RecipeStepsFormSet(
            queryset=RecipeSteps.objects.none(), prefix="steps"
        )

    context = {
        "form": form,
        "ingredient_formset": ingredient_formset,
        "steps_formset": steps_formset,
        "search_form": search_form,
    }
    return render(request, "recipes/create.html", context)


@login_required
def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    search_form = SearchForm()
    query = request.GET.get("query")
    recipes = Recipe.objects.all()

    if query:
        recipes = recipes.filter(
            Q(title__icontains=query) | Q(author__username__icontains=query)
        )
        if recipes.exists():
            return redirect("detail", id=recipes.first().id)

    if request.user != recipe.author:
        messages.error(request, "You aren't the author!")
        return redirect("detail", id=id)

    IngredientsFormSet = modelformset_factory(
        Ingredients,
        form=IngredientsForm,
        extra=1,
        can_delete=True,
        fields=["amount", "food_item"],
    )
    RecipeStepsFormSet = modelformset_factory(
        RecipeSteps,
        form=RecipeStepsForm,
        extra=1,
        can_delete=True,
        fields=["step_number", "instruction"],
    )

    if request.method == "POST":
        form = RecipeForm(request.POST, request.FILES, instance=recipe)
        ingredient_formset = IngredientsFormSet(
            request.POST,
            queryset=Ingredients.objects.filter(recipe=recipe),
            prefix="ingredients",
        )
        steps_formset = RecipeStepsFormSet(
            request.POST,
            queryset=RecipeSteps.objects.filter(recipe=recipe),
            prefix="steps",
        )

        if (
            form.is_valid()
            and ingredient_formset.is_valid()
            and steps_formset.is_valid()
        ):
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()

            for ingredient_form in ingredient_formset:
                if (
                    ingredient_form.cleaned_data
                    and not ingredient_form.cleaned_data.get("DELETE")
                ):
                    ingredient = ingredient_form.save(False)
                    ingredient.recipe = recipe
                    ingredient.save()
                elif (
                    ingredient_form.cleaned_data.get("DELETE")
                    and ingredient_form.instance.pk
                ):
                    ingredient_form.instance.delete()

            for steps_form in steps_formset:
                if steps_form.cleaned_data and not steps_form.cleaned_data.get(
                    "DELETE"
                ):
                    step = steps_form.save(False)
                    step.recipe = recipe
                    step.save()
                elif steps_form.cleaned_data.get("DELETE") and steps_form.instance.pk:
                    steps_form.instance.delete()

            return redirect("detail", id=id)
    else:
        form = RecipeForm(instance=recipe)
        ingredient_formset = IngredientsFormSet(
            queryset=Ingredients.objects.filter(recipe=recipe), prefix="ingredients"
        )
        steps_formset = RecipeStepsFormSet(
            queryset=RecipeSteps.objects.filter(recipe=recipe), prefix="steps"
        )

    context = {
        "recipe_object": recipe,
        "form": form,
        "ingredient_formset": ingredient_formset,
        "steps_formset": steps_formset,
        "search_form": search_form,
    }
    return render(request, "recipes/edit.html", context)


@login_required
def add_favorite(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    favorite, created = FavoriteRecipes.objects.get_or_create(
        user=request.user, recipe=recipe
    )
    if created:
        messages.success(request, f"{recipe.title} has been added to your favorites.")
    else:
        messages.info(request, f"{recipe.title} is already in your favorites.")
    return redirect("detail", id=recipe.id)


@login_required
def remove_favorite(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    favorite = FavoriteRecipes.objects.filter(user=request.user, recipe=recipe)
    if favorite.exists():
        favorite.delete()
    return redirect("favorites")


@login_required
def my_favorites(request):
    favorites = FavoriteRecipes.objects.filter(user=request.user)
    search_form = SearchForm()
    query = request.GET.get("query")
    recipes = Recipe.objects.all()

    if query:
        recipes = recipes.filter(
            Q(title__icontains=query) | Q(author__username__icontains=query)
        )
        if recipes.exists():
            return redirect("detail", id=recipes.first().id)
    context = {
        "favorites": favorites,
        "search_form": search_form,
    }
    return render(request, "recipes/favorites.html", context)
