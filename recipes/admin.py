from django.contrib import admin
from recipes.models import Recipe, FavoriteRecipes, RecipeSteps, Ingredients, Comment


@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = [
        "title",
        "id",
    ]


@admin.register(RecipeSteps)
class RecipeStepsAdmin(admin.ModelAdmin):
    list_display = [
        "step_number",
        "instruction",
        "id",
    ]


@admin.register(Ingredients)
class IngredientsAdmin(admin.ModelAdmin):
    list_display = [
        "food_item",
        "amount",
        "id",
    ]


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = [
        "text",
        "created_on",
        "author",
        "recipe",
        "id",
    ]


@admin.register(FavoriteRecipes)
class FavoriteRecipesAdmin(admin.ModelAdmin):
    list_display = [
        "user",
        "recipe",
    ]
