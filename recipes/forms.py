from django.forms import ModelForm
from recipes.models import Recipe, RecipeSteps, Ingredients, Comment
from django import forms


class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
            "servings",
            "calories",
            "protein",
            "fat",
            "carbs",
        ]


class RecipeStepsForm(ModelForm):
    class Meta:
        model = RecipeSteps
        fields = [
            "step_number",
            "instruction",
        ]


class IngredientsForm(ModelForm):
    class Meta:
        model = Ingredients
        fields = [
            "amount",
            "food_item",
        ]


class SearchForm(forms.Form):
    query = forms.CharField(label="", max_length=100)


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ["text"]
        widgets = {
            "text": forms.Textarea(attrs={"placeholder": "Add a comment..."}),
        }
        labels = {
            "text": "",
        }
