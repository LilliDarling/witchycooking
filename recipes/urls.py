from django.urls import path
from recipes.views import (
    edit_recipe,
    show_recipe,
    recipe_list,
    create_recipe,
    my_recipes,
    add_favorite,
    remove_favorite,
    my_favorites,
)


urlpatterns = [
    path("", recipe_list, name="all"),
    path("<int:id>/", show_recipe, name="detail"),
    path("create/", create_recipe, name="create"),
    path("<int:id>/edit/", edit_recipe, name="edit"),
    path("mine/", my_recipes, name="mine"),
    path("<int:id>/add/", add_favorite, name="add"),
    path("<int:id>/remove/", remove_favorite, name="remove"),
    path("favorites/", my_favorites, name="favorites"),
]
